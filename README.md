# Book Manager - Desafio SoftDesign

Projeto desenvolvido como parte do processo seletivo da SoftDesign Oportunidade (Pessoa Desenvolvedora PHP / Laravel) 

![banner image](https://i.imgur.com/7tPId5b.png)

## Descrição

Aplicação desenvolvida tendo como regras de negocio:

* Tela de login
* Controle de acesso 
* CRUD de Livros
* Consumir api hgbrasil Weather para consultar clima na atual cidade

O projeto foi desenvolvido utilizando o framework Laravel, em sua versão  **7.29* com PHP **7.2.5*.



## Tecnologias

* PHP / Laravel
* Javascript / Jquery
* Sqlite

## Libs javascript

* Bootstrap
* Moment
* IziToast
* Pace
* DataTable
* Confirm.js
* PikaDay

## Requerimentos

* PHP
* Composer

# Instalação 

Na raiz do projeto, via cmd, execute

```bash
cp .env.example .env
```

# Sem docker

O projeto utiliza o docker para configuração do ambiente, porem se o docker não tiver disponivel a instalação pode ser feita sem o mesmo.

Na raiz do projeto via cmd execute: 

```bash
composer install
```
Com as bibliotecas instaladas, execute:

```bash
php artisan serve
```

# Com docker

Na raiz do projeto via cmd execute:

```bash
docker-compose up -d --build
```

Nas próximas vezes execute:

```bash
docker-compose up -d
```

URL de acesso:

```bash
http://localhost:9000
```
## Login

Para fazer login na aplicação utilize as seguintes credenciais:

* Email : rafael@softdesign.com
* Senha: softdesign
 
## Contribuição
Pull Requests são bem vindos.

## License
For open source projects