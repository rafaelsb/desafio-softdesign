<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="600">

    <title>Login - Book Manager - Desafio SoftDesign</title>

    <!-- Styles -->
    <link href="{{ asset('styles/login.css') }}" rel="stylesheet">
    <link href="{{ asset('styles/global.css') }}" rel="stylesheet">
    @include('components.shared-css')

    <script src="{{ asset('js/login.js') }}"></script>
    @include('components.shared-js')

</head>

<body>
    <div id="page">
        <div id="page-content">
            <div class="logo-container">
                <img alt="Logo SoftDesign" src="{{ asset('img/brand.svg') }}" />
                <h2>Books Manager</h2>
            </div>
            <img class="brand-image" src="{{ asset('img/banner.svg') }}" />

            <form id="login">
                <div class="input-block">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" />
                </div>
                <div class="input-block">
                    <label for="password">Senha</label>
                    <input type="password" id="password" name="password" />
                </div>
                <div class="input-block">
                    <button onclick="login()" type="button">entrar</button>
                </div>
            </form>
        </div>
    </div>
</body>
<!--routes-->
<input type="hidden" id="loginRoute" value="{{ route('login') }}" />
<!--token-->
<input type="hidden" value="{{ csrf_token() }}" id="token" />

</html>
