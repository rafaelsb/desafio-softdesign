<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="600">

    <title>Book Manager - Desafio SoftDesign</title>

    @include('components.css')
    @include('components.shared-css')

    @include('components.shared-js')
    @include('components.js')

</head>

<body>

    <div id="page">
        <div id="page-account-content">
            <article class="account-item">
                <header>
                    <div class="profile-header">
                        <div class="profile-header-cover"></div>
                        <div class="profile-header-content">
                            <div class="profile-header-img mb-4">
                                <img src="{{ asset('img/brand.svg') }}" class="mb-4" alt="" />
                            </div>

                            <div class="profile-header-info">
                                <h4 class="m-t-sm">{{ auth()->user()->name }} <i onclick="logout()"
                                        data-toggle="tooltip" data-placement="top" title="sair"
                                        class="btn-tooltip fa fa-power-off"></i></h4>
                                <p class="m-b-sm">Analista de Sistemas</p>
                                <p class="m-b-sm" id="weather-info"></p>
                            </div>
                        </div>


                    </div>
                </header>
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 mx-auto">
                                <div class="mb-60">

                                    <form action="#" class="mb-60">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-3 my-3">
                                                <div class="input-group position-relative">
                                                    <input type="text" class="form-control filter"
                                                        placeholder="Titulo do Livro" id="title">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3 my-3">
                                                <div class="input-group position-relative">
                                                    <input type="text" class="form-control filter" placeholder="Autor"
                                                        id="author">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3 my-3">
                                                <div class="input-group position-relative">
                                                    <input type="text" class="form-control filter"
                                                        placeholder="Descrição" id="description">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3 my-3">
                                                <div class="input-group position-relative">
                                                    <input type="text" class="form-control filter"
                                                        placeholder="Qtd de paginas" id="number_pages">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3 my-3">
                                                <div class="input-group position-relative">
                                                    <input type="text" class="form-control filter"
                                                        placeholder="Data de cadastro" id="created_at">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3 my-3">
                                                <button type="button" class="btn btn-info" id="search">
                                                    buscar livros
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="container-fluid p-0">
                                   
                                    <div class="row">
                                        <div class="col-xl-8">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-actions float-right">
                                                        <h5 class="card-title mb-0">Livros</h5>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <table class="table table-striped" style="width:100%"
                                                        id="book-table"></table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-4" id="content-view">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-actions float-right">
                                                        <h5 class="card-title mb-0" id="title-view"></h5>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row g-0">
                                                        <div class="col-sm-9 col-xl-12 col-xxl-9">
                                                            <strong>Descrição</strong>
                                                            <p id="description-view"></p>
                                                        </div>
                                                    </div>

                                                    <table class="table table-sm mt-2 mb-4">
                                                        <tbody>
                                                            <tr>
                                                                <th>Autor</th>
                                                                <td>
                                                                    <p id="author-view"></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Quantidade de paginas</th>
                                                                <td>
                                                                    <p id="pages-view"></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Data de cadastro</th>
                                                                <td>
                                                                    <p id="created-at-view"></p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <div class="col-md-6 col-lg-12 my-3">
                                                        <button type="button"
                                                            class="input-group btn btn-block  btn-info"
                                                            onclick="clearForm()">
                                                            voltar
                                                        </button>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-4" id="content-register">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-actions float-right">
                                                        <h5 class="card-title mb-0" id="titleRegisterCard">Cadastrar novo
                                                            livro</h5>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <form id="book-register">
                                                        <div class="row">
                                                            <input type="hidden" id="idToEdit" />
                                                            <div class="col-md-6 col-lg-12 my-3">
                                                                <label>Título</label>
                                                                <div class="input-group position-relative">
                                                                    <input type="text" class="form-control"
                                                                        id="txt-title">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-lg-12 my-3">
                                                                <label>Autor</label>
                                                                <div class="input-group position-relative">
                                                                    <input type="text" class="form-control"
                                                                        id="txt-author">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-lg-12 my-3">
                                                                <label>Quantidade de Páginas</label>
                                                                <div class="input-group position-relative">
                                                                    <input type="number" class="form-control"
                                                                        id="number-number-pages">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-lg-12 my-3">
                                                                <label>Descrição</label>
                                                                <div class="input-group position-relative">
                                                                    <textarea type="text" class="form-control"
                                                                        id="txta-description"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-lg-12 my-3">
                                                                <button type="button"
                                                                    class="input-group btn btn-block  btn-info"
                                                                    id="save-btn" onclick="save()">
                                                                    cadastrar
                                                                </button>
                                                                <button type="button"
                                                                    class="input-group btn btn-block  btn-info"
                                                                    id="edit-btn" onclick="edit()">
                                                                    editar
                                                                </button>
                                                                <button type="button"
                                                                    class="input-group btn btn-block  btn-default"
                                                                    onclick="clearForm()">
                                                                    cancelar
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                </footer>
                <div class="actions">

                </div>
            </article>
        </div>
    </div>


    <!--rotas-->
    <input type="hidden" value="{{ route('search-book') }}" id="searchRoute" />
    <input type="hidden" value="{{ route('save-book') }}" id="saveRoute" />
    <input type="hidden" value="{{ route('edit-book') }}" id="editRoute" />
    <input type="hidden" value="{{ route('delete-book') }}" id="deleteRoute" />
    <!--token-->
    <input type="hidden" value="{{ csrf_token() }}" id="token" />
</body>
<html>
