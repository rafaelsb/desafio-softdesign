function showToastConfirm(title, content, callback) {
    $.confirm({
        title: title,
        content: content,
        theme: 'supervan',
        buttons: {
            sim: function() {
                callback();
            },
            cancelar: function() {

            },
        }
    });

}