//const api = 'https://api.hgbrasil.com/weather';

function getWeather(woeid) {
    $.ajax({
        url: `/weather?woeid=${woeid}`,
        method: 'get',
    }).fail(function(res) {
        console.error('Erro inesperado, não foi possivel buscar o clima da cidade informada!');
    }).done(function(res) {
        $('#weather-info').html(`${res.results.city} - Minima: ${res.results.forecast[0].min}° Maxima: ${res.results.forecast[0].max}° - ${res.results.description}`);
    });
}