var rules = [];
var rows = 10;
var idToRemove;
var created_at_formated

$(function() {

    getWeather('455826');

    new Pikaday({
        field: document.getElementById('created_at'),
        format: 'DD/MM/YYYY',
        onSelect: function() {
            created_at_formated = this.getMoment().format('Y-MM-DD');
        }
    });


    $('#search').click(function(e) {
        setRules();
        search({ rules: rules, rows: rows });
    });
    search({ rules: rules, rows: rows });
});

function search(search) {
    $('#book-table').DataTable({
        serverSide: true,
        searching: false,
        ordering: false,
        responsive: true,
        scrollX: true,
        ajax: {
            url: $('#searchRoute').val(),
            type: 'POST',
            data: {...search, _token: $('#token').val() },
        },
        columns: [
            { title: "Titulo", witdh: "20%" },
            { title: "Descrição", witdh: "30%" },
            { title: "Autor", witdh: "20%" },
            { title: "Páginas", witdh: "15%" },
            { title: "Cadastro", witdh: "15%" },
            { title: "" }
        ],

        columnDefs: [{
            render: function(data, type, row) {
                return `<button data-toggle="tooltip" data-placement="top" title="editar" class="btn-tooltip btn btn-block btn-sm btn-warning" onclick="toEdit(${data})"><i class="fa fa-edit"></i></button> 
                <button data-toggle="tooltip" data-placement="top" title="excluir" class="btn-tooltip btn btn-block btn-sm btn-danger" onclick="confirmDelete(${data})"><i class="fa fa-trash"></i></button>
                 <button data-toggle="tooltip" data-placement="top" title="detalhes" class="btn-tooltip btn btn-block btn-sm btn-info" onclick="details(${data})"><i class="fa fa-eye"></i></button>`;
            },
            targets: 5
        }]
    });
    $('.btn-tooltip').tooltip();
}

function setRules() {

    $('#book-table').DataTable().destroy();

    rules = [];
    var inputs = $(".filter");

    for (var i = 0; i < inputs.length; i++) {
        if ($(inputs[i]).val() != '') {
            var obj = {
                column: $(inputs[i]).attr('id'),
                relation: $(inputs[i]).attr('id') != 'created_at' ? 'contains' : 'equal',
                value: $(inputs[i]).attr('id') == 'created_at' ? created_at_formated : $(inputs[i]).val()
            };
            rules.push(obj);
        }
    }
}

function save() {
    const book = {
        title: $('#txt-title').val(),
        author: $('#txt-author').val(),
        description: $('#txta-description').val(),
        pages: $('#number-number-pages').val()
    }
    $.ajax({
        url: $('#saveRoute').val(),
        method: 'post',
        data: {...book, _token: $('#token').val() }
    }).fail(function(res) {
        iziToast.error({ title: 'Aviso', message: 'Erro inesperado, cadastro não realizado!' });
    }).done(function(res) {
        if (!res.success) {
            res.msg.forEach(msg => {
                iziToast.error({ title: 'Aviso', message: msg, position: 'bottomLeft' });
            });
            return false;
        }
        setRules();
        search({ rules: rules, rows: rows });
        clearForm();
        iziToast.success({ title: 'ok!', message: res.msg });
    });
}

function toEdit(id) {
    $('#titleRegisterCard').html("Editar livro");
    $('#edit-btn').show();
    $('#save-btn').hide();

    $.ajax({
        url: `/book/${id}`,
        method: 'get',
    }).fail(function(res) {
        iziToast.error('error', 'Erro', 'Erro inesperado, livro não encontrado!');
    }).done(function(res) {
        if (!res.success) {
            res.msg.forEach(msg => {
                iziToast.error({ title: 'Aviso', message: msg, position: 'bottomLeft' });
            });
            return false;
        }
        $('#txt-title').val(res.book.title);
        $('#txta-description').val(res.book.description);
        $('#txt-author').val(res.book.author);
        $('#number-number-pages').val(res.book.number_pages);
        $('#idToEdit').val(res.book.id);
        backFromView();

    });
}

function edit() {

    const id = $('#idToEdit').val();

    const book = {
        title: $('#txt-title').val(),
        author: $('#txt-author').val(),
        description: $('#txta-description').val(),
        pages: $('#number-number-pages').val()
    }

    $.ajax({
        url: `${$('#editRoute').val()}?id=${id}`,
        method: 'put',
        data: {...book, _token: $('#token').val() }
    }).fail(function(res) {
        iziToast.error({ title: 'Aviso', message: 'Erro inesperado, edição não realizada!' });
    }).done(function(res) {
        if (!res.success) {
            res.msg.forEach(msg => {
                iziToast.error({ title: 'Aviso', message: msg, position: 'bottomLeft' });
            });
            return false;
        }
        setRules();
        search({ rules: rules, rows: rows });
        clearForm();
        iziToast.success({ title: 'ok!', message: res.msg });
    });
}

function confirmDelete(id) {
    idToRemove = id;
    showToastConfirm('Exclusão', 'ação irreversivel', remove);
}

function remove() {
    $.ajax({
        url: `${$('#deleteRoute').val()}?id=${idToRemove}`,
        method: 'delete',
        data: { _token: $('#token').val() }
    }).fail(function(res) {
        iziToast.error({ title: 'Aviso', message: 'Erro inesperado, exclusão não realizada!' });
    }).done(function(res) {
        if (!res.success) {
            res.msg.forEach(msg => {
                iziToast.error({ title: 'Aviso', message: msg, position: 'bottomLeft' });
            });
            return false;
        }
        setRules();
        search({ rules: rules, rows: rows });

        iziToast.success({ title: 'ok!', message: res.msg });
    });
}

function details(id) {
    $.ajax({
        url: `/book/${id}`,
        method: 'get',
    }).fail(function(res) {
        iziToast.error('error', 'Erro', 'Erro inesperado, livro não encontrado!');
    }).done(function(res) {
        if (!res.success) {
            res.msg.forEach(msg => {
                iziToast.error({ title: 'Aviso', message: msg, position: 'bottomLeft' });
            });
            return false;
        }
        $('#title-view').html(res.book.title);
        $('#description-view').html(res.book.description);
        $('#author-view').html(res.book.author);
        $('#pages-view').html(res.book.number_pages);
        $('#created-at-view').html(res.book.created_at);
        $('#content-register').hide();
        $('#content-view').show();
    });
}

function clearForm() {
    $('#book-register').trigger('reset');
    $('#titleRegisterCard').html("Cadastrar novo livro");
    $('#edit-btn').hide();
    $('#save-btn').show();
    backFromView();
}

function backFromView() {
    $('#content-register').show();
    $('#content-view').hide();
}

function logout() {
    $.ajax({
        url: '/logout',
        method: 'get',
    }).fail(function(res) {
        iziToast.error('error', 'Erro', 'Erro inesperado, não foi possivel efetuar o logout!');
    }).done(function(res) {
        iziToast.success({ title: 'ok!', message: 'Logout realizado, aguarde redirecionamento!', position: 'bottomLeft' });
        window.location.href = '/login';
    });
}