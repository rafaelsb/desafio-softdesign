function login() {

    let login = {
        email: $('#email').val(),
        password: $('#password').val(),
        _token: $('#token').val()
    }

    $.ajax({
        url: $('#loginRoute').val(),
        method: 'post',
        data: login
    }).fail(function(res) {
        iziToast.error({ title: 'Aviso', message: 'Erro inesperado, login não realizado!' });
    }).done(function(res) {
        if (!res.success) {
            res.msg.forEach(msg => {
                iziToast.error({ title: 'Aviso', message: msg, position: 'bottomLeft' });
            });
            return false;
        }

        iziToast.success({ title: 'ok!', message: 'Login realizado, aguarde redirecionamento!', position: 'bottomLeft' });
        window.location.href = '/';

    });

}