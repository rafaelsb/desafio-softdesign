<?php

namespace App\Services;

use DB;
use App\Model\Book;
use App\Repository\BookRepository;
use DateTime;

class BookService
{
  private $errors = [];
  protected $repository;



  public function __construct(BookRepository $bookRepository)
  {
    $this->repository = $bookRepository;
  }


  public function save($request)
  {
    $book = Book::populateToSave($request);
    return $this->repository->save($book);
  }

  public function update($request)
  {
    $book = Book::populateToUpdate($request);
    return $this->repository->update($book);
  }

  public function delete($id)
  {
    $book = $this->repository->get($id);
    if (gettype($book) != 'object') {
      return $book;
    }
    return $this->repository->delete($book);
  }

  public function get($id)
  {
    $book =  $this->repository->get($id);
    if (gettype($book) != 'object') {
      return $book;
    }

    return ['success' => true, 'book' => Book::populateToView($book)];
  }

  public function getFromFilter($request)
  {
    $result = $this->repository->search($request);
    return Book::populateToList($request->draw, $result['books']->toArray(), $result['count']);
  }
}
