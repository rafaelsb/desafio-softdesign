<?php

namespace App\Model;

use App\Interfaces\BookInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;

use function PHPSTORM_META\map;

class Book extends Model implements BookInterface
{
    protected $table = 'book';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public static function populateToSave(Request $request)
    {
        $book = new Self();
        $book->title = $request->title;
        $book->description = $request->description;
        $book->author = $request->author;
        $book->number_pages = $request->pages;
        $book->created_at = date('Y-m-d');
        return $book;
    }

    public static function populateToUpdate(Request $request)
    {
        $book = Book::find($request->id);
        $book->title = $request->title;
        $book->description = $request->description;
        $book->author = $request->author;
        $book->number_pages = $request->pages;
        $book->update_at = date('Y-m-d');
        return $book;
    }

    public static function populateToView(Model $book)
    {
        $book->created_at = Carbon::parse($book->created_at)->format('d/m/Y');
        if ($book->update_at) {
            $book->update_at = Carbon::parse($book->update_at)->format('d/m/Y');
        }
        return $book;
    }

    public static function populateToList($draw, $books, $total)
    {

        $items =  array_map(function ($book) {
            $book['created_at'] = Carbon::parse($book['created_at'])->format('d/m/Y');

            return array(
                $book['title'],
                substr($book['description'],0,30).'...',
                $book['author'],
                $book['number_pages'],
                $book['created_at'],
                $book['id'],
            );
        }, $books);

        return array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $items
        );
    }
}
