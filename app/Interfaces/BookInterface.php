<?php

namespace App\Interfaces;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;


interface BookInterface{
    public static function populateToSave(Request $request);
    public static function populateToUpdate(Request $request);
    public static function populateToList($draw,$books,$total);
    public static function populateToView(Model $model);
}