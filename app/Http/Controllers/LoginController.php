<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return  response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false, 'msg' => ['Credenciais Incorretas']]);
        }
    }

    public function logout()
    {
        Auth::logout();
    }
}
