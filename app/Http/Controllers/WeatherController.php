<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WeatherController extends Controller
{
    /**
     * Retorna informações do clima da cidade solicitada.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function getWeather()
    {
        
        $cid =  json_decode(file_get_contents('https://api.hgbrasil.com/geoip?key='.env('HG_KEY').'&address=remote&precision=false'), true); // busca pela cidade atravez do IP
        return  json_decode(file_get_contents('https://api.hgbrasil.com/weather?woeid='.$cid['results']['woeid']), true); // Recebe os dados da cidade informada
        
    }
}
