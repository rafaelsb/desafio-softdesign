<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\BookService;
use App\Http\Requests\BookRequest;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{

  protected $service;

  public function __construct()
  {
    $this->service = app(BookService::class);
  }

  /**
   *save a new book.
   *
   * @param  Request  $request
   * @return Json Object
   */
  public function save(Request $request)
  {
    $validator = Validator::make(
      $request->all(),
      (new BookRequest())->rules(),
      (new BookRequest)->messages()
    );

    if ($validator->fails()) {
      return response()->json([
        'success' => false,
        'msg' => $validator->getMessageBag()->all()
      ]);
    }

    return response()->json($this->service->save($request));
  }

  /**
   *update a  book.
   *
   * @param  Request  $request
   * @return Json Object
   */
  public function update(Request $request)
  {
    $validator = Validator::make(
      $request->all(),
      (new BookRequest())->rules(),
      (new BookRequest)->messages()
    );

    if ($validator->fails()) {
      return response()->json([
        'success' => false,
        'msg' => $validator->getMessageBag()->all()
      ]);
    }

    return response()->json($this->service->update($request));
  }

  /**
   *delete a  book.
   *
   * @param  Request  $request
   * @return Json Object
   */
  public function delete(Request $request)
  {
    return response()->json($this->service->delete($request->id));
  }
  
  
  /**
   *get a  book.
   *
   * @param  Number  $id
   * @return Json Object
   */
  public function get($id){
    return response()->json($this->service->get($id));
  }

  /**
   *get books with filter and pagination.
   *
   * @param  Request  $request
   * @return Json Object
   */
  public function getFromFilter(Request $request){
    return response()->json($this->service->getFromFilter($request));
  }

}
