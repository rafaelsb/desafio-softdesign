<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Model\Book;
use DateTime;

class BookRepository
{
    private $bookEloquentModel;

    public function __construct(Book $bookModel)
    {
        $this->bookEloquentModel = $bookModel;
    }


    private $operators = [
        "equal" => '=',
        "greater" => '>',
        "greater_or_equal" => '>=',
        "less" => '<',
        "less_or_equal" => '<=',
        "contains" => 'LIKE',
    ];

    public function save(Model $book)
    {
        DB::beginTransaction();
        try {
            $book->save();
            DB::commit();
            return  ['success' => true, 'msg' => 'Livro Cadastro!'];
        } catch (\PDOException $exc) {
            DB::rollback();
            $err = $exc->getMessage();
            return ['success' => false, 'err' => $err, 'msg' => ['Não foi possivel salvar este livro']];
        }
    }

    public function update(Model $book)
    {
        DB::beginTransaction();
        try {
            $book->save();
            DB::commit();
            return  ['success' => true, 'msg' => 'Livro Atualizado!'];;
        } catch (\PDOException $exc) {
            DB::rollback();
            $err = $exc->getMessage();
            return ['success' => false, 'err' => $err, 'msg' => ['Não foi possivel atualizar este livro.']];
        }
    }

    public function delete(Model $book)
    {
        DB::beginTransaction();
        try {
            $book->delete();
            DB::commit();
            return  ['success' => true, 'msg' => 'Livro Removido!'];;
        } catch (\PDOException $exc) {
            DB::rollback();
            $err = $exc->getMessage();
            return ['success' => false, 'err' => $err, 'msg' => ['Não foi possivel remover este livro.']];
        }
    }

    public function get($id)
    {
        $book = $this->bookEloquentModel::find($id);
        if (!$book) {
            return ['success' => false, 'msg' => ['Livro não encontrado.']];
        }
        return $book;
    }

    public function search($request)
    {
        $books =  $this->bookEloquentModel::orderby('id', 'desc');
        if (isset($request->rules)) {
            foreach ($request->rules as $key => $value) {
                if ($this->operators[$value['relation']] == 'LIKE') {
                    $books->where($value['column'], $this->operators[$value['relation']], '%' . $value['value'] . '%');
                } else {
                    $books->where($value['column'], $this->operators[$value['relation']], $value['value']);
                }
            }
        }

        $books = $books->select('*');
        $count = $books->count();
        return ['books' => $books->skip(intVal($request->start))
            ->take(intVal($request->length))->get(), 'count' => $count];
    }
}
