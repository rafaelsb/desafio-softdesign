<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', ['as' => 'login', function () {
    return view('login');
}]);

Route::post('/login', ['as' => 'login', 'uses' => 'LoginController@authenticate']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', ['as' => 'dash', function () {
        return view('book');
    }]);

    Route::get('/logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);

    Route::post('/book', ['as' => 'save-book', 'uses' => 'BookController@save']);
    Route::put('/book', ['as' => 'edit-book', 'uses' => 'BookController@update']);
    Route::delete('/book', ['as' => 'delete-book', 'uses' => 'BookController@delete']);
    Route::get('/book/{id}', ['as' => 'get-book', 'uses' => 'BookController@get']);
    Route::post('/book/search', ['as' => 'search-book', 'uses' => 'BookController@getFromFilter']);


    Route::get('/weather', ['as' => 'weather', 'uses' => 'WeatherController@getWeather']);
});
